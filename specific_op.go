package gexpr

type op_peeker struct {
	runes []rune
	value string
}

func new_op_peeker(src string) op_peeker {
	var runes []rune
	for _, chr := range src {
		runes = append(runes, chr)
	}
	return op_peeker{runes: runes, value: src}
}

func (self *op_peeker) CanPeek(seq []rune) int {
	for i, chr := range self.runes {
		if i >= len(seq) || chr != seq[i] {
			return 0
		}
	}
	return len(self.runes)
}

func apply_peekers(peekers []op_peeker, src []rune) (string, []rune) {
	candidate_index := -1
	candidate_len := 0
	for i, peeker := range peekers {
		p_len := peeker.CanPeek(src)
		if p_len > candidate_len {
			candidate_index = i
			candidate_len = p_len
		}
	}
	if candidate_index < 0 {
		return "", src
	} else {
		op := peekers[candidate_index].value
		return op, src[candidate_len:]
	}
}
