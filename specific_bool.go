package gexpr

type bool_node struct {
	base_node
	value bool
}

type bool_node_parser struct{}

func (bool_node_parser) parse(_ IParser, src []rune) (INode, []rune) {
	check := func(word []rune) bool {
		if len(src) < len(word) {
			return false
		}
		for i, chr := range word {
			if src[i] != chr {
				return false
			}
		}
		return true
	}
	if check(true_word) {
		return bool_node{value: true}, src[len(true_word):]
	}
	if check(false_word) {
		return bool_node{value: false}, src[len(false_word):]
	}
	return nil, nil
}

func (bool_node) Type() NodeType {
	return NodeTypeBool
}

func (self bool_node) Raw() string {
	return self.ToString()
}

func (self bool_node) Bool() bool {
	return self.value
}

func (self bool_node) ToString() string {
	if self.value {
		return string(true_word)
	} else {
		return string(false_word)
	}
}
