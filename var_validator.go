package gexpr

type var_validator struct {
	funcs          map[string]int
	consts         map[string]int
	allow_var_val  bool
	allow_var_call bool
}

func new_var_validator(config ParserConfig) iVarValidator {
	make_map := func(m map[string]int, sources ...[]string) map[string]int {
		result := make(map[string]int)
		for key := range m {
			result[key] = 1
		}
		for _, src := range sources {
			for _, key := range src {
				result[key] = 1
			}
		}
		return result
	}
	return &var_validator{
		funcs:          make_map(config.BinOperators, config.Funcs, config.UnOperators),
		consts:         make_map(map[string]int{}, config.Consts),
		allow_var_val:  config.AllowVarVal,
		allow_var_call: config.AllowVarCall,
	}
}

func (self *var_validator) validate(node INode) error {
	switch node.Type() {
	case NodeTypeVar:
		return self.validate_var(node)
	case NodeTypeCall:
		return self.validate_call(node)
	case NodeTypeMap:
		for _, val := range node.Map() {
			if err := self.validate(val); err != nil {
				return err
			}
		}
	case NodeTypeArr:
		return self.validate_values(node.Arr())
	}
	return nil
}

func (self *var_validator) validate_var(node INode) error {
	if self.allow_var_val {
		return nil
	}
	name := node.VarName()
	_, found := self.consts[name]
	if !found {
		return &ErrUnknownValue{name: name}
	}
	return nil
}

func (self *var_validator) validate_call(node INode) error {
	if self.allow_var_call {
		return self.validate_values(node.Args())
	}
	name := node.Func()
	_, found := self.funcs[name]
	if !found {
		return &ErrUnknownFunc{name: name}
	}
	return self.validate_values(node.Args())
}

func (self *var_validator) validate_values(nodes []INode) error {
	for _, node := range nodes {
		if err := self.validate(node); err != nil {
			return err
		}
	}
	return nil
}
