package gexpr

import "fmt"

type ErrUnparsableSeq struct{ txt string }

type ErrEndOfSeq struct{}

type ErrUnexpectedEndOfSeq struct{}

type ErrDelimeter struct{ expected bool }

type ErrCanNotParseValue struct{ txt string }

type ErrExpectedToken struct{ token string }

type ErrUnknownValue struct{ name string }

type ErrUnknownFunc struct{ name string }

func isExpectedDelim(err error) bool {
	if err == nil {
		return false
	}
	err_delim, casted := err.(*ErrDelimeter)
	if !casted {
		return false
	}
	return err_delim.expected
}

func (e ErrUnparsableSeq) Error() string {
	return "Unparsable sequence: " + e.txt
}

func (e ErrEndOfSeq) Error() string {
	return "Found end of sequence"
}

func (e ErrUnexpectedEndOfSeq) Error() string {
	return "Unexpected end of sequence"
}

func (e ErrDelimeter) Error() string {
	if e.expected {
		return "Found delimeter(expected)"
	} else {
		return "Found delimeter(unexpected)"
	}
}

func (e ErrCanNotParseValue) Error() string {
	return fmt.Sprintf("Can not parse value at: \"%s\"", e.txt)
}

func (e ErrExpectedToken) Error() string {
	return fmt.Sprintf("Expected token: %s", e.token)
}

func (e ErrUnknownValue) Error() string {
	return fmt.Sprintf("Unknown value: %s", e.name)
}

func (e ErrUnknownFunc) Error() string {
	return fmt.Sprintf("Unknown func: %s", e.name)
}
