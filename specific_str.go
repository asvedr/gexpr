package gexpr

type str_node struct {
	base_node
	data string
	raw  string
}

type str_node_parser struct{}

func (str_node_parser) parse(_ IParser, src []rune) (INode, []rune) {
	if len(src) == 0 || src[0] != '"' {
		return nil, nil
	}
	var data string
	raw := "\""
	in_escape := false
	src = src[1:]
	for i, chr := range src {
		raw += string(chr)
		if in_escape {
			switch chr {
			case '"':
				data += "\""
			case '\\':
				data += "\\"
			case 'n':
				data += "\n"
			case 't':
				data += "\t"
			default:
				return nil, nil
			}
			in_escape = false
		} else {
			switch chr {
			case '"':
				return &str_node{data: data, raw: raw}, src[i+1:]
			case '\\':
				in_escape = true
			default:
				data += string(chr)
			}
		}
	}
	return nil, nil
}

func (str_node) Type() NodeType {
	return NodeTypeStr
}

func (self *str_node) Raw() string {
	return self.raw
}

func (self *str_node) StringVal() string {
	return self.data
}

func (self *str_node) ToString() string {
	return self.Raw()
}
