package gexpr

import "fmt"

type NodeType int

const (
	NodeTypeInt NodeType = iota
	NodeTypeReal
	NodeTypeStr
	NodeTypeBool
	NodeTypeVar
	NodeTypeCall
	NodeTypeMap
	NodeTypeArr
)

type ParserConfig struct {
	BinOperators map[string]int
	UnOperators  []string
	Funcs        []string
	Consts       []string
	VarAlphabet  string
	DenyInt      bool
	DenyReal     bool
	DenyStr      bool
	AllowBool    bool
	AllowVarVal  bool
	AllowVarCall bool
	AllowMap     bool
	AllowArr     bool
}

func (self NodeType) String() string {
	switch self {
	case NodeTypeInt:
		return "TypeInt"
	case NodeTypeReal:
		return "TypeReal"
	case NodeTypeStr:
		return "TypeStr"
	case NodeTypeBool:
		return "TypeBool"
	case NodeTypeVar:
		return "TypeVar"
	case NodeTypeCall:
		return "TypeCall"
	case NodeTypeMap:
		return "TypeMap"
	case NodeTypeArr:
		return "TypeArr"
	default:
		panic(fmt.Sprintf("unexpected type: %d", self))
	}
}
