package gexpr

import (
	"fmt"
)

type parser struct {
	specifics      []iSpecificParser
	bin_ops        map[string]int
	bin_op_peekers []op_peeker
	un_ops         []string
	un_op_peekers  []op_peeker
	validator      iVarValidator
}

type parser_token struct {
	operator string
	value    INode
}

func (pt parser_token) String() string {
	if pt.value != nil {
		return "val(" + pt.value.ToString() + ")"
	} else {
		return "op(" + pt.operator + ")"
	}
}

func NewParser(config ParserConfig) IParser {
	var un_op_peekers, bin_op_peekers []op_peeker
	for _, name := range config.UnOperators {
		un_op_peekers = append(un_op_peekers, new_op_peeker(name))
	}
	for name := range config.BinOperators {
		bin_op_peekers = append(bin_op_peekers, new_op_peeker(name))
	}
	return &parser{
		specifics:      make_specifics(config),
		bin_ops:        config.BinOperators,
		bin_op_peekers: bin_op_peekers,
		un_ops:         config.UnOperators,
		un_op_peekers:  un_op_peekers,
		validator:      new_var_validator(config),
	}
}

func (self *parser) Parse(src string) (INode, error) {
	var runes []rune
	for _, chr := range src {
		runes = append(runes, chr)
	}
	runes = append(runes, ' ')
	val, rest, err := self.parse_runes(runes)
	if len(rest) != 0 {
		return nil, &ErrUnparsableSeq{txt: string(rest)}
	}
	if err != nil {
		return nil, err
	}
	return val, self.validator.validate(val)
}

func (self *parser) parse_runes(runes []rune) (INode, []rune, error) {
	tokens, rest, err := self.collect_tokens(runes)
	if !isExpectedDelim(err) {
		return nil, rest, err
	}
	node, err := self.build_tokens(tokens)
	return node, rest, err
}

func (self *parser) collect_tokens(runes []rune) ([]parser_token, []rune, error) {
	var tokens []parser_token
	for {
		runes = skip_space(runes)
		if check_end_or_delim(runes) {
			return tokens, runes, &ErrDelimeter{expected: false}
		}
		val, rest, err := self.take_value(runes)
		if err != nil {
			return nil, runes, err
		}
		tokens = append(tokens, parser_token{value: val})
		runes = skip_space(rest)
		if check_end_or_delim(runes) {
			return tokens, runes, &ErrDelimeter{expected: true}
		}
		op, rest := self.take_bin_op(runes)
		if len(op) == 0 {
			return tokens, runes, nil
		}
		tokens = append(tokens, parser_token{operator: op})
		runes = rest
	}
}

func (self *parser) build_tokens(tokens []parser_token) (INode, error) {
	switch len(tokens) {
	case 0:
		return nil, &ErrUnexpectedEndOfSeq{}
	case 1:
		if tokens[0].value != nil {
			return tokens[0].value, nil
		} else {
			panic(fmt.Sprintf("single token is operator: %v", tokens))
		}
	}
	left_tokens, op, right_tokens := self.split_on_min_op(tokens)
	left, err := self.build_tokens(left_tokens)
	if err != nil {
		return nil, err
	}
	right, err := self.build_tokens(right_tokens)
	if err != nil {
		return nil, err
	}
	node := &call_node{fun: op, args: []INode{left, right}}
	return node, err
}

func (self *parser) split_on_min_op(tokens []parser_token) ([]parser_token, string, []parser_token) {
	op_index := -1
	op_prior := -1
	for i, token := range tokens {
		if token.value != nil {
			continue
		}
		new_prior := self.bin_ops[token.operator]
		if op_index < 0 || new_prior <= op_prior {
			op_index = i
			op_prior = new_prior
		}
	}
	if op_index < 0 || op_index%2 == 0 {
		panic(fmt.Sprintf("Invalid token list: %v", tokens))
	}
	return tokens[:op_index], tokens[op_index].operator, tokens[op_index+1:]
}

func (self *parser) take_value(runes []rune) (INode, []rune, error) {
	runes = skip_space(runes)
	if len(runes) == 0 {
		return nil, nil, &ErrEndOfSeq{}
	}
	un_op, rest := self.take_un_op(runes)
	if len(un_op) != 0 {
		arg, rest, err := self.take_value(rest)
		if err != nil {
			return nil, nil, err
		}
		un_call := &call_node{
			fun:  un_op,
			args: []INode{arg},
			mode: call_mode_un,
		}
		return un_call, rest, nil
	}
	if runes[0] == start_call {
		val, rest, err := self.parse_runes(runes[1:])
		if err != nil {
			return nil, nil, err
		}
		if len(rest) == 0 {
			return nil, nil, &ErrUnexpectedEndOfSeq{}
		}
		if rest[0] != end_call {
			return nil, nil, &ErrExpectedToken{token: string(end_call)}
		}
		return val, rest[1:], nil
	}
	return self.take_bare_value(runes)
}

func (self *parser) take_bare_value(runes []rune) (INode, []rune, error) {
	var best_value INode
	var min_rest []rune
	for _, specific := range self.specifics {
		value, rest := specific.parse(self, runes)
		if value == nil {
			continue
		}
		if best_value == nil || len(rest) < len(min_rest) {
			best_value = value
			min_rest = rest
		}
	}
	if best_value == nil {
		return nil, runes, &ErrCanNotParseValue{txt: string(runes)}
	}
	return best_value, min_rest, nil
}

func (self *parser) take_un_op(runes []rune) (string, []rune) {
	return apply_peekers(self.un_op_peekers, runes)
}

func (self *parser) take_bin_op(runes []rune) (string, []rune) {
	return apply_peekers(self.bin_op_peekers, runes)
}

func check_end_or_delim(runes []rune) bool {
	if len(runes) == 0 {
		return true
	}
	_, is_delim := delims[runes[0]]
	if is_delim {
		return true
	}
	return false
}

func skip_space(runes []rune) []rune {
	for i, r := range runes {
		_, found := spaces[r]
		if !found {
			return runes[i:]
		}
	}
	return []rune{}
}

func make_specifics(config ParserConfig) []iSpecificParser {
	var_parser := new_var_node_parser(config.VarAlphabet)
	int_parser := &int_node_parser{}
	str_parser := &str_node_parser{}
	result := []iSpecificParser{&call_node_parser{var_parser: var_parser}}
	if !config.DenyInt {
		result = append(result, int_parser)
	}
	if !config.DenyReal {
		result = append(result, &real_node_parser{int_parser: int_parser})
	}
	if !config.DenyStr {
		result = append(result, str_parser)
	}
	if config.AllowBool {
		result = append(result, &bool_node_parser{})
	}
	if config.AllowMap {
		result = append(result, &map_node_parser{str_parser: str_parser})
	}
	if config.AllowArr {
		result = append(result, &arr_node_parser{})
	}
	return append(result, var_parser)
}
