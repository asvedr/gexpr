package gexpr_test

import "testing"

func TestMap0(t *testing.T) {
	val, err := parser().Parse("{}")
	if err != nil {
		t.Fatal(err)
	}
	if len(val.Map()) != 0 {
		t.Fatal(val.ToString())
	}
}

func TestMap2(t *testing.T) {
	val, err := parser().Parse(`{"a": 1, "b": 2}`)
	if err != nil {
		t.Fatal(err)
	}
	if len(val.Map()) != 2 {
		t.Fatal(val.ToString())
	}
	if val.Map()["a"].Int() != 1 {
		t.Fatal(val.Map()["a"].ToString())
	}
	if val.Map()["b"].Int() != 2 {
		t.Fatal(val.Map()["b"].ToString())
	}
}
