package gexpr_test

import (
	"testing"
)

func TestInt(t *testing.T) {
	val, err := parser().Parse("1")
	if err != nil {
		t.Fatal(err)
	}
	if val.Int() != 1 {
		t.Fatal(val.ToString())
	}
	val, err = parser().Parse(" 1 ")
	if err != nil {
		t.Fatal(err)
	}
	if val.Int() != 1 {
		t.Fatal(val.ToString())
	}
}

func TestFloat(t *testing.T) {
	val, err := parser().Parse("30.05")
	if err != nil {
		t.Fatal(err)
	}
	if !(val.Real() > 30.04 && val.Real() < 30.06) {
		t.Fatal(val.ToString())
	}
	val, err = parser().Parse(" 2. ")
	if err != nil {
		t.Fatal(err)
	}
	if !(val.Real() > 1.99 && val.Real() < 2.01) {
		t.Fatal(val.ToString())
	}
}

func TestStr(t *testing.T) {
	val, err := parser().Parse(`"str"`)
	if err != nil {
		t.Fatal(err)
	}
	if val.StringVal() != "str" {
		t.Fatal(val.ToString())
	}
	val, err = parser().Parse(` "str" `)
	if err != nil {
		t.Fatal(err)
	}
	if val.StringVal() != "str" {
		t.Fatal(val.ToString())
	}
	val, err = parser().Parse(`"a\"b\n"`)
	if err != nil {
		t.Fatal(err)
	}
	if val.StringVal() != "a\"b\n" {
		t.Fatal(val.ToString())
	}
}

func TestBool(t *testing.T) {
	val, err := parser().Parse("true")
	if err != nil {
		t.Fatal(err)
	}
	if !val.Bool() {
		t.Fatal(val.ToString())
	}
	val, err = parser().Parse(" false ")
	if err != nil {
		t.Fatal(err)
	}
	if val.Bool() {
		t.Fatal(val.ToString())
	}
}

func TestVar(t *testing.T) {
	val, err := parser().Parse("x")
	if err != nil {
		t.Fatal(err)
	}
	if val.VarName() != "x" {
		t.Fatal(val.ToString())
	}
	val, err = parser().Parse(" Ab_ ")
	if err != nil {
		t.Fatal(err)
	}
	if val.VarName() != "Ab_" {
		t.Fatal(val.ToString())
	}
}
