package gexpr_test

import (
	"testing"

	"gitlab.com/asvedr/gexpr"
)

func TestBrackets(t *testing.T) {
	val, err := parser().Parse("f([- ( 1 ) + a], ((2)))")
	if err != nil {
		t.Fatal(err)
	}
	if val.ToString() != "f([+(-(1),a)],2)" {
		t.Fatal(val.ToString())
	}
}

func TestDenyVars(t *testing.T) {
	c := gexpr.ParserConfig{
		BinOperators: map[string]int{"*": 1, "and": 0},
		Consts:       []string{"pi"},
	}
	p := gexpr.NewParser(c)

	val, err := p.Parse("pi * 2")
	if err != nil {
		t.Fatal(err)
	}
	if val.ToString() != "*(pi,2)" {
		t.Fatal(val.ToString())
	}

	_, err = p.Parse("and * 2")
	if err == nil || err.Error() != "Unknown value: and" {
		t.Fatal(err)
	}
}

func TestDenyFuncs(t *testing.T) {
	c := gexpr.ParserConfig{
		BinOperators: map[string]int{"*": 1, "and": 0},
		Consts:       []string{"pi"},
		Funcs:        []string{"sqrt"},
	}
	p := gexpr.NewParser(c)

	val, err := p.Parse("sqrt(2*2) and pi")
	if err != nil {
		t.Fatal(err)
	}
	if val.ToString() != "and(sqrt(*(2,2)),pi)" {
		t.Fatal(val.ToString())
	}

	_, err = p.Parse("pi(2)")
	if err == nil || err.Error() != "Unknown func: pi" {
		t.Fatal(err)
	}
}
