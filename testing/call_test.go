package gexpr_test

import "testing"

func TestCallNone(t *testing.T) {
	val, err := parser().Parse("f()")
	if err != nil {
		t.Fatal(err)
	}
	if val.Func() != "f" || len(val.Args()) != 0 {
		t.Fatal(val.ToString())
	}
}

func TestCall3(t *testing.T) {
	val, err := parser().Parse(`fun(a, 1, "x")`)
	if err != nil {
		t.Fatal(err)
	}
	if val.Func() != "fun" {
		t.Fatal(val.ToString())
	}
	if val.Args()[0].VarName() != "a" {
		t.Fatal(val.Args()[0].ToString())
	}
	if val.Args()[1].Int() != 1 {
		t.Fatal(val.Args()[1].ToString())
	}
	if val.Args()[2].StringVal() != "x" {
		t.Fatal(val.Args()[2].ToString())
	}
}
