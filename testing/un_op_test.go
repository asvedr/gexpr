package gexpr_test

import (
	"testing"
)

func TestNeg(t *testing.T) {
	val, err := parser().Parse("-2")
	if err != nil {
		t.Fatal(err)
	}
	if val.Func() != "-" || len(val.Args()) != 1 {
		t.Fatal(val.ToString())
	}
	if val.Args()[0].Int() != 2 {
		t.Fatal(val)
	}
}

func TestDoubleNeg(t *testing.T) {
	val, err := parser().Parse("--2")
	if err != nil {
		t.Fatal(err)
	}
	if val.Func() != "-" || len(val.Args()) != 1 {
		t.Fatal(val.ToString())
	}
	if val.Args()[0].Func() != "-" || len(val.Args()[0].Args()) != 1 {
		t.Fatal(val.Args()[0].ToString())
	}
	if val.Args()[0].Args()[0].Int() != 2 {
		t.Fatal(val.Args()[0].Args()[0].ToString())
	}
}
