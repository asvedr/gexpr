package gexpr_test

import "gitlab.com/asvedr/gexpr"

func parser() gexpr.IParser {
	c := gexpr.ParserConfig{
		BinOperators: map[string]int{
			"+":   1,
			"-":   1,
			"+=":  1,
			"*":   2,
			"and": 0,
		},
		UnOperators:  []string{"-"},
		AllowBool:    true,
		AllowVarVal:  true,
		AllowVarCall: true,
		AllowMap:     true,
		AllowArr:     true,
	}
	return gexpr.NewParser(c)
}
