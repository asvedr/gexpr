package gexpr_test

import "testing"

func TestArr0(t *testing.T) {
	val, err := parser().Parse("[]")
	if err != nil {
		t.Fatal(err)
	}
	if len(val.Arr()) != 0 {
		t.Fatal(val.ToString())
	}
}

func TestArr2(t *testing.T) {
	val, err := parser().Parse("[1,2]")
	if err != nil {
		t.Fatal(err)
	}
	if len(val.Arr()) != 2 {
		t.Fatal(val.ToString())
	}
	if val.Arr()[0].Int() != 1 {
		t.Fatal(val.Arr()[0].ToString())
	}
	if val.Arr()[1].Int() != 2 {
		t.Fatal(val.Arr()[1].ToString())
	}
}
