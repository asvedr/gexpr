package gexpr_test

import "testing"

func TestAPlusB(t *testing.T) {
	val, err := parser().Parse("a + b")
	if err != nil {
		t.Fatal(err)
	}
	if val.Func() != "+" {
		t.Fatal(val.Func())
	}
	if val.Args()[0].VarName() != "a" {
		t.Fatal(val.Args()[0])
	}
	if val.Args()[1].VarName() != "b" {
		t.Fatal(val.Args()[1])
	}
}

func TestAPlusEqB(t *testing.T) {
	val, err := parser().Parse("a+=b")
	if err != nil {
		t.Fatal(err)
	}
	if val.Func() != "+=" {
		t.Fatal(val.Func())
	}
	if val.Args()[0].VarName() != "a" {
		t.Fatal(val.Args()[0])
	}
	if val.Args()[1].VarName() != "b" {
		t.Fatal(val.Args()[1])
	}
}

func TestAandB(t *testing.T) {
	val, err := parser().Parse("a and b")
	if err != nil {
		t.Fatal(err)
	}
	if val.Func() != "and" {
		t.Fatal(val.Func())
	}
	if val.Args()[0].VarName() != "a" {
		t.Fatal(val.Args()[0])
	}
	if val.Args()[1].VarName() != "b" {
		t.Fatal(val.Args()[1])
	}
}

func TestPriorSame(t *testing.T) {
	val, err := parser().Parse("1 -2 - 3")
	if err != nil {
		t.Fatal(err)
	}
	if val.ToString() != "-(-(1,2),3)" {
		t.Fatal(val.ToString())
	}
}

func TestPriorBrackets(t *testing.T) {
	val, err := parser().Parse("1-(2-3)")
	if err != nil {
		t.Fatal(err)
	}
	if val.ToString() != "-(1,-(2,3))" {
		t.Fatal(val.ToString())
	}
}

func TestPriorDiff(t *testing.T) {
	val, err := parser().Parse("1 -2 * 3 and  4")
	if err != nil {
		t.Fatal(err)
	}
	if val.ToString() != "and(-(1,*(2,3)),4)" {
		t.Fatal(val.ToString())
	}
}
