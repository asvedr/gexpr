package gexpr

import (
	"fmt"
	"strings"
)

type arr_node struct {
	base_node
	items []INode
}

type arr_node_parser struct{}

func (arr_node_parser) parse(parser IParser, src []rune) (INode, []rune) {
	if len(src) == 0 || src[0] != start_arr {
		return nil, nil
	}
	src = skip_space(src[1:])
	if len(src) == 0 {
		return nil, nil
	}
	var items []INode
	if src[0] == end_arr {
		return &arr_node{items: items}, src[1:]
	}
	for {
		item, rest, err := parser.parse_runes(src)
		if err != nil {
			return nil, nil
		}
		items = append(items, item)
		src = skip_space(rest)
		if len(src) == 0 {
			return nil, nil
		}
		switch src[0] {
		case sep:
			src = skip_space(src[1:])
		case end_arr:
			return &arr_node{items: items}, src[1:]
		default:
			return nil, nil
		}
	}
}

func (arr_node) Type() NodeType {
	return NodeTypeArr
}

func (self arr_node) Raw() string {
	var items []string
	for _, item := range self.items {
		items = append(items, item.Raw())
	}
	return fmt.Sprintf("[%s]", strings.Join(items, ","))
}

func (self arr_node) Arr() []INode {
	return self.items
}

func (self arr_node) ToString() string {
	var items []string
	for _, item := range self.items {
		items = append(items, item.ToString())
	}
	return fmt.Sprintf("[%s]", strings.Join(items, ","))
}
