package gexpr

import (
	"fmt"
	"strings"
)

type call_node struct {
	base_node
	fun  string
	args []INode
	mode call_mode
}

type call_mode int

const (
	call_mode_fun call_mode = iota
	call_mode_bin
	call_mode_un
)

type call_node_parser struct {
	var_parser iSpecificParser
}

func (self *call_node_parser) parse(parser IParser, src []rune) (INode, []rune) {
	fun_node, rest := self.var_parser.parse(parser, src)
	if fun_node == nil {
		return nil, nil
	}
	fun := fun_node.VarName()
	src = skip_space(rest)
	if len(src) == 0 || src[0] != start_call {
		return nil, nil
	}
	src = src[1:]
	var args []INode
	src = skip_space(src)
	if len(src) == 0 {
		return nil, nil
	}
	if src[0] == end_call {
		return &call_node{fun: fun, args: args, mode: call_mode_fun}, src[1:]
	}
	for {
		arg, rest, err := parser.parse_runes(src)
		if err != nil {
			return nil, nil
		}
		args = append(args, arg)
		src = skip_space(rest)
		if len(src) == 0 {
			return nil, nil
		}
		switch src[0] {
		case end_call:
			return &call_node{fun: fun, args: args, mode: call_mode_fun}, src[1:]
		case sep:
			src = src[1:]
		default:
			return nil, nil
		}
	}
}

func (call_node) Type() NodeType {
	return NodeTypeCall
}

func (self *call_node) Raw() string {
	switch self.mode {
	case call_mode_fun:
		var args []string
		for _, arg := range self.args {
			args = append(args, arg.Raw())
		}
		return fmt.Sprintf("%s(%s)", self.fun, strings.Join(args, ","))
	case call_mode_bin:
		return fmt.Sprintf(
			"%s %s %s",
			self.args[0].Raw(),
			self.fun,
			self.args[1].Raw(),
		)
	case call_mode_un:
		return self.fun + self.args[0].Raw()
	default:
		panic(fmt.Sprintf("unexpected call mode: %d", self.mode))
	}
}

func (self *call_node) RawValue() string {
	return self.ToString()
}

func (self *call_node) Args() []INode {
	return self.args
}

func (self *call_node) Func() string {
	return self.fun
}

func (self *call_node) ToString() string {
	var args []string
	for _, arg := range self.args {
		args = append(args, arg.ToString())
	}
	return fmt.Sprintf("%s(%s)", self.fun, strings.Join(args, ","))
}
