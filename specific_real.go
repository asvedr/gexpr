package gexpr

import (
	"fmt"
	"strconv"
)

type real_node struct {
	base_node
	src string
}

type real_node_parser struct {
	int_parser iSpecificParser
}

func (self real_node_parser) parse(p IParser, src []rune) (INode, []rune) {
	build := func(a string, b string) INode {
		return &real_node{src: fmt.Sprintf("%s.%s", a, b)}
	}
	first, rest := self.int_parser.parse(p, src)
	if first == nil {
		return nil, nil
	}
	if len(rest) == 0 || rest[0] != '.' {
		return build(first.Raw(), "0"), rest
	}
	src = rest[1:]
	second, rest := self.int_parser.parse(p, src)
	if second == nil {
		return build(first.Raw(), "0"), src
	} else {
		return build(first.Raw(), second.Raw()), rest
	}
}

func (real_node) Type() NodeType {
	return NodeTypeReal
}

func (self *real_node) Raw() string {
	return self.src
}

func (self *real_node) Real() float64 {
	val, _ := strconv.ParseFloat(self.src, 64)
	return val
}

func (self *real_node) ToString() string {
	return self.src
}
