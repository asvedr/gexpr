package gexpr

var spaces = map[rune]int{' ': 1, '\n': 1, '\t': 1, '\r': 1}

const start_call = '('
const end_call = ')'
const start_arr = '['
const end_arr = ']'
const start_map = '{'
const key_val_sep = ':'
const end_map = '}'
const sep = ','

var true_word = []rune{'t', 'r', 'u', 'e'}
var false_word = []rune{'f', 'a', 'l', 's', 'e'}

var delims = map[rune]int{end_call: 1, end_map: 1, end_arr: 1, sep: 1}
