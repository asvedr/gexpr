package gexpr

type var_node struct {
	base_node
	name string
}

type var_node_parser struct {
	alphabet map[rune]int
}

func default_var_alphabet() map[rune]int {
	a_map := map[rune]int{'_': 1}
	for i := int('a'); i <= int('z'); i += 1 {
		a_map[rune(i)] = 1
	}
	for i := int('A'); i <= int('Z'); i += 1 {
		a_map[rune(i)] = 1
	}
	return a_map
}

func new_var_node_parser(alphabet string) iSpecificParser {
	if len(alphabet) == 0 {
		return &var_node_parser{alphabet: default_var_alphabet()}
	}
	a_map := map[rune]int{}
	for _, chr := range alphabet {
		a_map[chr] = 1
	}
	return &var_node_parser{alphabet: a_map}
}

func (self *var_node_parser) parse(_ IParser, src []rune) (INode, []rune) {
	build := func(at int) (INode, []rune) {
		if at == 0 {
			return nil, nil
		}
		return &var_node{name: string(src[:at])}, src[at:]
	}
	for i, chr := range src {
		_, matched := self.alphabet[chr]
		if !matched {
			return build(i)
		}
	}
	return build(len(src))
}

func (var_node) Type() NodeType {
	return NodeTypeVar
}

func (self *var_node) Raw() string {
	return self.name
}

func (self *var_node) VarName() string {
	return self.name
}

func (self *var_node) ToString() string {
	return self.name
}
