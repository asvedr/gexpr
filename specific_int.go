package gexpr

import (
	"fmt"
	"strconv"
)

type int_node struct {
	base_node
	data int64
	raw  string
}

type int_node_parser struct{}

func (int_node_parser) parse(_ IParser, src []rune) (INode, []rune) {
	build := func(at int) (INode, []rune) {
		if at == 0 {
			return nil, nil
		}
		raw := string(src[:at])
		data, err := strconv.ParseInt(raw, 10, 64)
		if err != nil {
			return nil, nil
		}
		return &int_node{data: data, raw: raw}, src[at:]
	}
	for i, chr := range src {
		if chr < '0' || chr > '9' {
			return build(i)
		}
	}
	return build(len(src))
}

func (int_node) Type() NodeType {
	return NodeTypeInt
}

func (self *int_node) Int() int64 {
	return self.data
}

func (self *int_node) Raw() string {
	return self.raw
}

func (self *int_node) ToString() string {
	return fmt.Sprint(self.data)
}
