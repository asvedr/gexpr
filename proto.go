package gexpr

type INode interface {
	Type() NodeType
	Raw() string
	Int() int64
	Real() float64
	Bool() bool
	StringVal() string
	Args() []INode
	Func() string
	VarName() string
	Arr() []INode
	Map() map[string]INode
	ToString() string
}

type base_node struct{}

func (base_node) Int() int64            { panic("not implemented") }
func (base_node) Real() float64         { panic("not implemented") }
func (base_node) Bool() bool            { panic("not implemented") }
func (base_node) StringVal() string     { panic("not implemented") }
func (base_node) Args() []INode         { panic("not implemented") }
func (base_node) Func() string          { panic("not implemented") }
func (base_node) VarName() string       { panic("not implemented") }
func (base_node) Arr() []INode          { panic("not implemented") }
func (base_node) Map() map[string]INode { panic("not implemented") }

type iSpecificParser interface {
	parse(IParser, []rune) (INode, []rune)
}

type iVarValidator interface {
	validate(INode) error
}

type IParser interface {
	Parse(src string) (INode, error)
	parse_runes(runes []rune) (INode, []rune, error)
}
