package gexpr

import (
	"fmt"
	"strings"
)

type map_node struct {
	base_node
	data map[string]INode
}

type map_node_parser struct {
	str_parser iSpecificParser
}

func (self map_node_parser) parse(parser IParser, src []rune) (INode, []rune) {
	if len(src) == 0 || src[0] != start_map {
		return nil, nil
	}
	src = skip_space(src[1:])
	if len(src) == 0 {
		return nil, nil
	}
	data := map[string]INode{}
	if src[0] == end_map {
		return &map_node{data: data}, src[1:]
	}
	for {
		key, rest := self.str_parser.parse(parser, src)
		if key == nil {
			return nil, nil
		}
		src = skip_space(rest)
		if len(src) == 0 || src[0] != key_val_sep {
			return nil, nil
		}
		val, rest, err := parser.parse_runes(src[1:])
		if err != nil {
			return nil, nil
		}
		data[key.StringVal()] = val
		src = skip_space(rest)
		switch src[0] {
		case sep:
			src = skip_space(src[1:])
		case end_map:
			return &map_node{data: data}, src[1:]
		default:
			return nil, nil
		}
	}
}

func (map_node) Type() NodeType {
	return NodeTypeMap
}

func (self *map_node) Raw() string {
	var items []string
	for key, val := range self.data {
		fmt.Sprintf("\"%s\": %s", key, val.Raw())
	}
	return fmt.Sprintf("{%s}", strings.Join(items, ","))
}

func (self *map_node) Map() map[string]INode {
	return self.data
}

func (self *map_node) ToString() string {
	var items []string
	for key, val := range self.data {
		fmt.Sprintf("\"%s\": %s", key, val.ToString())
	}
	return fmt.Sprintf("{%s}", strings.Join(items, ","))
}
